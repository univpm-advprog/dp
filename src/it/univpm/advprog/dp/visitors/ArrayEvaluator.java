package it.univpm.advprog.dp.visitors;

import java.util.Arrays;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.Minus;
import it.univpm.advprog.dp.expressions.Multiply;
import it.univpm.advprog.dp.expressions.Plus;
import it.univpm.advprog.dp.expressions.Visitor;
import it.univpm.advprog.dp.expressions.arrays.ArrayValue;

public class ArrayEvaluator extends Evaluator<Integer[]> {
	
	Integer[] value ;
	
	public Integer[] getValue() {
		return value;
	}
	
	public AValue<Integer[]> getResult() {
		return new ArrayValue(this.getValue());
	}
	
	
	public void reset() {
		this.value = null;
	}
	
	private Integer[] arraySum(Integer[] a, Integer[] b) throws Exception {
		
		if (a != null && b != null && a.length != b.length) {
			throw new Exception("The two arrays must have same length");
		} 
		
		Integer[] res = null;
		
		if (a == null) {
			res = b;
		} else if (b == null) {
			res = a;
		} else {
			res = new Integer[a.length];
			
			for (Integer i = 0; i < a.length; i++) {
				res[i] = a[i] + b[i];
			}
		}
		return res;
	}
	
	private Integer[] arrayDiff(Integer[] a, Integer[] b) throws Exception {
		
		if (a != null && b != null && a.length != b.length) {
			throw new Exception("The two arrays must have same length");
		} 
		
		Integer[] res = null;
		
		if (b == null) {
			res = a;
		} else {
			if (a == null) {
				//a = new Integer[b.length];
				res = Arrays.copyOf(b, b.length);
				//Arrays.fill(a, 0);
			} else {
					
				res = new Integer[a.length];
				
				for (Integer i = 0; i < a.length; i++) {
					res[i] = a[i] - b[i];
				}
			}
		}
		return res;
	}
	
	@Override
	public void visitValue(AValue<Integer[]> v) {
		this.value = v.getValue(); // TODO 
	}

	@Override
	public void visitPlus(Plus<Integer[]> o) {

		try {
			for (Expression<Integer[]> curr : o.getArguments()) {
				ArrayEvaluator e = new ArrayEvaluator();
				curr.acceptVisitor(e);
				
				value = arraySum(value, e.getValue());
			} 
		
		} catch (Exception e) {
			this.logError("The operation (" + this.toString() + ") contains an error", e);
		}

	}

	@Override
	public void visitMinus(Minus<Integer[]> o) {
		
		try {
			for (Expression<Integer[]> curr : o.getArguments()) {
				ArrayEvaluator e = new ArrayEvaluator();
				curr.acceptVisitor(e);
				
				value = arrayDiff(value, e.getValue());
			} 
		
		} catch (Exception e) {
			this.logError("The operation (" + this.toString() + ") contains an error", e);
		}
	}

	private Integer[] arrayMultiply(Integer[] a, Integer[] b) throws Exception {
		
		if (a != null && b != null && a.length != b.length) {
			throw new Exception("The two arrays must have same length");
		} 
		
		Integer[] res = null;
		
		if (a == null) {
			res = b;
		} else if (b == null) {
			res = a;
		} else {
			res = new Integer[a.length];
			
			for (Integer i = 0; i < a.length; i++) {
				res[i] = a[i] * b[i];
			}
		}
		return res;
	}

	
	@Override
	public void visitMultiply(Multiply<Integer[]> o) {
		try {
			value = null;
			for (Expression<Integer[]> curr : o.getArguments()) {
				ArrayEvaluator e = new ArrayEvaluator();
				curr.acceptVisitor(e);
				
				value = arrayMultiply(value, e.getValue());
			} 
		
		} catch (Exception e) {
			this.logError("The operation (" + this.toString() + ") contains an error", e);
		}
	}




}
