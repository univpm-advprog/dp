package it.univpm.advprog.dp.visitors;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.Minus;
import it.univpm.advprog.dp.expressions.Multiply;
import it.univpm.advprog.dp.expressions.Operation;
import it.univpm.advprog.dp.expressions.Plus;
import it.univpm.advprog.dp.expressions.Visitor;

public class SumValues extends Visitor<Integer> {

	Integer accumulator = 0;
	
	@Override
	public void visitValue(AValue<Integer> v) {
		this.accumulator += v.getValue();
	}

	@Override
	public void visitPlus(Plus<Integer> o) {
		for (Expression<Integer> curr : o.getArguments()) {
			curr.acceptVisitor(this);
		}
	}

	@Override
	public void visitMinus(Minus<Integer> o) {
		for (Expression<Integer> curr : o.getArguments()) {
			curr.acceptVisitor(this);
		}		
	}
	
	public Integer getResult() {
		return this.accumulator;
	}

	@Override
	public void reset() {
		this.accumulator = 0;
	}

	@Override
	public void visitMultiply(Multiply<Integer> o) {
		for (Expression<Integer> curr : o.getArguments()) {
			curr.acceptVisitor(this);
		}		
	}


}
