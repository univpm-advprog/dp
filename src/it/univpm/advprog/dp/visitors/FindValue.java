package it.univpm.advprog.dp.visitors;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.Minus;
import it.univpm.advprog.dp.expressions.Multiply;
import it.univpm.advprog.dp.expressions.Operation;
import it.univpm.advprog.dp.expressions.Plus;
import it.univpm.advprog.dp.expressions.Visitor;
import it.univpm.advprog.dp.expressions.integers.IntValue;
import it.univpm.advprog.dp.factories.IntFactory;

public class FindValue extends Visitor<Integer> {
	
	AValue<Integer> toSearch;
	AValue<Integer> found;
	
	public FindValue(AValue<Integer> toSearch) {
		this.toSearch = toSearch;
	}
	
	public FindValue(Integer toSearch) {
		IntFactory f = new IntFactory();
		this.toSearch = f.createValue(toSearch);
	}

	
	@Override
	public void visitValue(AValue<Integer> v) {
		if (toSearch.getValue() == v.getValue()) {
			this.found = v;
		}
	}

	public void visit(Operation<Integer> o) {
		for (Expression<Integer> curr : o.getArguments()) {
			curr.acceptVisitor(this);
			if (this.found != null) {
				return;
			}
		}
	}
	
	@Override
	public void visitPlus(Plus<Integer> o) {
		this.visit(o);
	}

	@Override
	public void visitMinus(Minus<Integer> o) {
		this.visit(o);
	}

	public AValue<Integer> getFound() {
		return this.found;
	}
	
	public boolean isFound() {
		return this.found != null;
	}

	@Override
	public void reset() {
		this.found = null;
	}

	@Override
	public void visitMultiply(Multiply<Integer> o) {
		this.visit(o);
	}
}
