package it.univpm.advprog.dp.visitors;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.Minus;
import it.univpm.advprog.dp.expressions.Multiply;
import it.univpm.advprog.dp.expressions.Plus;
import it.univpm.advprog.dp.expressions.Visitor;
import it.univpm.advprog.dp.expressions.arrays.ArrayValue;
import it.univpm.advprog.dp.expressions.integers.IntValue;

public class IntEvaluator extends Evaluator<Integer> {
	
	Integer value = 0 ;
	
	public Integer getValue() {
		return value;
	}
	
	public AValue<Integer> getResult() {
		return new IntValue(this.getValue());
	}
	
	public void reset() {
		this.value = 0;
	}
	
	@Override
	public void visitValue(AValue<Integer> v) {
		this.value = v.getValue(); // TODO 
	}

	@Override
	public void visitPlus(Plus<Integer> o) {
		for (Expression<Integer> curr : o.getArguments()) {
			IntEvaluator e = new IntEvaluator();
			curr.acceptVisitor(e);
			value = value + e.getValue();
		}
	}

	@Override
	public void visitMinus(Minus<Integer> o) {
		
		Expression<Integer>[] arguments = o.getArguments();
		
		for (int pos=0; pos<arguments.length; pos++) {
			IntEvaluator e = new IntEvaluator();
			arguments[pos].acceptVisitor(e);
			
			if (pos == 0) {
				this.value += e.getValue();
			} else {
				this.value -= e.getValue();
			}
		}
	}

	@Override
	public void visitMultiply(Multiply<Integer> o) {
		this.value = 1;
		
		for (Expression<Integer> curr : o.getArguments()) {
			IntEvaluator e = new IntEvaluator();
			curr.acceptVisitor(e);
			value = value * e.getValue();
		}
	}




}
