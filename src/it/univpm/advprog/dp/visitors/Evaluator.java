package it.univpm.advprog.dp.visitors;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Visitor;

public abstract class Evaluator<T> extends Visitor<T> {

	public abstract AValue<T> getResult();
}
