package it.univpm.advprog.dp.app;

import java.util.logging.Level;
import java.util.logging.Logger;

import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.arrays.ArrayPlus;
import it.univpm.advprog.dp.expressions.arrays.ArrayValue;
import it.univpm.advprog.dp.expressions.integers.IntMinus;
import it.univpm.advprog.dp.expressions.integers.IntPlus;
import it.univpm.advprog.dp.expressions.integers.IntValue;
import it.univpm.advprog.dp.factories.ArrayFactory;
import it.univpm.advprog.dp.factories.Factory;
import it.univpm.advprog.dp.factories.IntFactory;
import it.univpm.advprog.dp.visitors.IntEvaluator;
import it.univpm.advprog.dp.visitors.ArrayEvaluator;
import it.univpm.advprog.dp.visitors.FindValue;
import it.univpm.advprog.dp.visitors.SumValues;

public class AppTest {

	static Logger logger = Logger.getAnonymousLogger();
	
	public static void main(String[] args) {
		IntEvaluator eval = new IntEvaluator();
		ArrayEvaluator arEval = new ArrayEvaluator();
		
		// test 1
		
		Expression<Integer> exp1 = new IntPlus(new IntValue(3), new IntValue(5));
		exp1.acceptVisitor(eval);
		
		System.out.println("Test 1: " + exp1.toString() + " = " + eval.getResult());

		
		// test 2
		eval.reset();
		Expression<Integer> exp2 = new IntPlus(new IntValue(3), new IntValue(5), new IntValue(-8)) ;
		exp2.acceptVisitor(eval);
		System.out.println("Test 2: " + exp2.toString() + " = " + eval.getResult());
		
		// test 2.a
		eval.reset();
		Expression<Integer> exp2a = new IntMinus(new IntValue(3), new IntPlus(new IntValue(3), new IntValue(5)), new IntValue(-8)) ;
		exp2a.acceptVisitor(eval);
		System.out.println("Test 2a: " + exp2a.toString() + " = " + eval.getResult());


		// test 2.b
		Integer[] arguments = new Integer[3];
		arguments[0] = 3;
		arguments[1] = 4;
		arguments[2] = 5;
		ArrayValue v = new ArrayValue(arguments);
		System.out.println("Test 2b: " + v.toString());
		
		// test 2.c
		Integer[] arguments2 = new Integer[3];
		arguments2[0] = 1;
		arguments2[1] = 2;
		arguments2[2] = 3;
		ArrayValue v2 = new ArrayValue(arguments2);
		
		Expression<Integer[]> exp2c = new ArrayPlus(v, v2);
		
		exp2c.acceptVisitor(arEval);
		System.out.println("Test 2c: " + exp2c.toString() + " = " + arEval.getResult());

		
		// test 3 
		eval.reset();
		Factory intFactory = new IntFactory();
		
		try {
			Expression exp3 = intFactory.createOperation("+", intFactory.createValue("3"), intFactory.createValue("8"), intFactory.createValue("10")); //arguments3);
			exp3.acceptVisitor(eval);
			System.out.println("Test 3: " + exp3.toString() + " = " + eval.getResult());
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Parsing error", e);
		}
		
		// test 4 
		Factory arFactory = new ArrayFactory();
		try {
			arEval.reset();
			Expression exp4 = arFactory.createOperation("-", arFactory.createValue("3"), arFactory.createValue("8"), arFactory.createValue("10"));
			exp4.acceptVisitor(arEval);
			System.out.println("Test 4: " + exp4.toString() + " = " + arEval.getResult());
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Parsing error", e);
		}
		
		// test 5 (parse array with arFactory => OK)
		try {
			arEval.reset();
			Expression exp5 = arFactory.createOperation("+", arFactory.createValue("3,4,5"), arFactory.createValue("8,9,10"), arFactory.createValue("10,11,12"));
			exp5.acceptVisitor(arEval);
			System.out.println("Test 5: " + exp5.toString() + " = " + arEval.getResult());
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Parsing error", e);
		}
		// test 6 (parse array with intFactory => ERR)
		
		try {
			arEval.reset();
			Expression exp6 = arFactory.createOperation("+", intFactory.createValue("3,4,5"), intFactory.createValue("8,9,10"), intFactory.createValue("10,11,12"));
			System.out.println("Test 6: " + exp6.toString() + " = " + arEval.getResult());
		} catch (Exception e) {
			System.out.println("Test 6: a parsing error happened (it was expected, see the STDERR for details)");
			logger.log(Level.SEVERE, "Parsing error", e);
		}
		
		// test 7
		try {
			// NB qui il visitor manipola solo interi, quindi devo usare intFactory
			eval.reset();
			Expression exp7 = intFactory.createOperation("+", intFactory.createValue("10"), intFactory.createValue("20"), intFactory.createValue("30"));
			exp7.acceptVisitor(eval);
			System.out.println("Test 7: " + exp7.toString() + " = " + eval.getResult());

			SumValues vSum = new SumValues();
			exp7.acceptVisitor(vSum);
			System.out.println("Test 7: values of expression sum to : " + vSum.getResult());

			FindValue vFind = new FindValue(10);
			exp7.acceptVisitor(vFind);
			System.out.println("Test 7: value 10 is contained in wrapper with hash: " + vFind.getFound().hashCode());

			
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Parsing error", e);
		}

		try {
			eval.reset();
			Expression exp8 = intFactory.createOperation("+", intFactory.createValue("10"), intFactory.createOperation("-", intFactory.createValue("20"), intFactory.createValue("40")), intFactory.createValue("30"));
			exp8.acceptVisitor(eval);
			System.out.println("Test 8: " + exp8.toString() + " = " + eval.getResult());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		
	}

}
