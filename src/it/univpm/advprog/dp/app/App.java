package it.univpm.advprog.dp.app;

import picocli.CommandLine;
//import picocli.CommandLine.Command; // commented because we use a class named Command in the it.univpm.advprog.dp.controller package
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.Callable;
import it.univpm.advprog.dp.controller.Command;
import it.univpm.advprog.dp.controller.EvaluateExpression;
import it.univpm.advprog.dp.controller.LogToFile;
import it.univpm.advprog.dp.controller.SelectMode;
import it.univpm.advprog.dp.expressions.parser.BinaryOperationParser;
import it.univpm.advprog.dp.expressions.parser.Parser;
import it.univpm.advprog.dp.model.Model;
import it.univpm.advprog.dp.model.Model.MODE;
import it.univpm.advprog.dp.views.Prompt;
import it.univpm.advprog.dp.views.View;

// reference for the "picocli" library:
// https://picocli.info/

@CommandLine.Command(name = "calc", mixinStandardHelpOptions = true, version = "calc 0.1",
         description = "run an  interactive calculator.")
class App implements Callable<Integer> {

    @Parameters(arity="0", index = "0", description = "The expression.")
    private String exp; 

    @Option(names = {"-m", "--mode"}, description = "int, array, ...")
    private String mode = "int";
    
    private Model model = new Model();
    private View view = null;
    private Parser parser = new BinaryOperationParser();
    
    // this example implements Callable, so parsing, error handling and handling user
    // requests for usage help or version help can be done with one line of code.
    public static void main(String... args) {
        int exitCode = new CommandLine(new App()).execute(args);
        System.exit(exitCode);
    }
    
    
    @Override
    public Integer call() throws Exception {
    	
    	if (this.mode.equals("int")) {
    		this.model.setMode(MODE.integer);
    	} else if (this.mode.equals("array")) {
    		this.model.setMode(MODE.array);
    	} else {
    		throw new Exception("Mode unrecognized: " + this.mode);
    	}

    	if (this.exp != null && this.exp.length() > 0) 
    	{
			this.model.appendToHistory(this.exp);
			
			// NB here we don't need a view, because we don't expect to interact with the user
			EvaluateExpression evaluate = new EvaluateExpression();
			evaluate.setParser(parser);
			evaluate.execute(this.model);
    	} 
    	else 
    	{
    		this.view = new Prompt(this.parser, ">>");
    		view.update(this.model);
    	}
    	
		return 0;
	
    }
    
}