package it.univpm.advprog.dp.views;

import it.univpm.advprog.dp.controller.Command;
import it.univpm.advprog.dp.model.Model;

public class History extends Command {

	@Override
	public void execute(Model model) {
		for (String curr : model.getHistory()) {
			model.print(curr);
		}
	}

}
