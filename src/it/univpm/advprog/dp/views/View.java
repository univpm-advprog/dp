package it.univpm.advprog.dp.views;

import it.univpm.advprog.dp.model.Model;

public abstract class View {
	public abstract void update(Model model);
}
