package it.univpm.advprog.dp.views;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import it.univpm.advprog.dp.controller.Command;
import it.univpm.advprog.dp.controller.EvaluateExpression;
import it.univpm.advprog.dp.controller.LogToFile;
import it.univpm.advprog.dp.controller.SelectMode;
import it.univpm.advprog.dp.expressions.parser.Parser;
import it.univpm.advprog.dp.model.Model;

public class Prompt extends View {

    Map<String, Command> commandRegistry = new HashMap<String, Command>();
    Command defaultCommand = new EvaluateExpression(); 
    String prompt = ">";
    
    public Prompt(Parser parser) {
    	// register commands
    	this.commandRegistry.put(":mode", new SelectMode());
    	this.commandRegistry.put(":log", new LogToFile()); 
    	this.commandRegistry.put(":history", new History());
    	
    	((EvaluateExpression)this.defaultCommand).setParser(parser);
    }
    
    public Prompt(Parser parser, String prompt) {
    	this(parser);
    	this.prompt = prompt;
    	((EvaluateExpression)this.defaultCommand).setParser(parser);
    }
    

	
	@Override
	public void update(Model model) {
    	// NB this is a special kind of try/finally that closes sc in case of exceptions
		
		try (Scanner sc = new Scanner(System.in)) 
    	{
	    	while (true) {
	    		System.out.print(this.prompt + " ");
	    		String text = sc.nextLine();
	    		
	    		if (text.length() == 0) {
	    			continue;
	    		}

    			model.appendToHistory(text);

	    		char marker = text.charAt(0);
	    		if (marker == ':') {
	    			// process prompt command
	    			this.runCommand(model, text);
	    		} else {
	    			// evaluate expression
	    			//this.evaluate(text);
	    			this.defaultCommand.execute(model);
	    		}
	    	}
    	} 

	}

    public void runCommand(Model model, String text) {
    	
    	String name = text;
    	int pos = text.indexOf(' ');
    	if (pos > 0) {
    		name = text.substring(0, pos);
    	}
    	
    	Command cmd = this.commandRegistry.getOrDefault(name, null);
    	
    	if (cmd == null)
    	{
    		throw new RuntimeException("Command not recognized (" + name + ")");
    	}
    	
    	cmd.execute(model);
    }


}
