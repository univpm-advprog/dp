package it.univpm.advprog.dp.controller;

import java.util.HashMap;
import java.util.Map;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.parser.BinaryOperationParser;
import it.univpm.advprog.dp.expressions.parser.Parser;
import it.univpm.advprog.dp.model.Model;
import it.univpm.advprog.dp.model.Model.MODE;
import it.univpm.advprog.dp.factories.ArrayFactory;
import it.univpm.advprog.dp.factories.Factory;
import it.univpm.advprog.dp.factories.IntFactory;
import it.univpm.advprog.dp.visitors.ArrayEvaluator;
import it.univpm.advprog.dp.visitors.Evaluator;
import it.univpm.advprog.dp.visitors.IntEvaluator;

public class EvaluateExpression extends Command {

	private Map<Model.MODE, Factory> factory = new HashMap<Model.MODE, Factory>();
    private Map<Model.MODE, Evaluator> evaluator = new HashMap<Model.MODE, Evaluator>();
    
    // TODO how to apply dependency-injection to this?
    private Parser parser; // = new BinaryOperationParser();
    
    public EvaluateExpression() {
    	
    	this.evaluator.put(MODE.integer, new IntEvaluator());
    	this.evaluator.put(MODE.array, new ArrayEvaluator());
    	
    	this.factory.put(MODE.integer, new IntFactory());
    	this.factory.put(MODE.array, new ArrayFactory());
    }
    
    public void setParser(Parser parser) {
    	this.parser = parser;
    }
    
	@Override
	public void execute(Model model) {
		try
		{
			String last = model.getLastInput();
			
	    	Evaluator e = this.evaluator.getOrDefault(model.getMode(), null);
	    	
	    	if (e == null) {
	    		throw new RuntimeException("No evaluator for mode: " + model.getMode());
	    	}
	    	
	    	Factory f = this.factory.getOrDefault(model.getMode(), null);
	    	
	    	if (f == null) {
	    		throw new RuntimeException("No factory for mode: " + model.getMode());
	    	}
	    	
	    	if (this.parser == null) {
	    		throw new RuntimeException("You must provide a parser, first");
	    	}
	    	
	    	this.parser.setFactory(f);
	    	
	    	Expression ast = this.parser.parse(last);
	    	
	    	e.reset();
	    	ast.acceptVisitor(e);
	    	
	    	AValue res = e.getResult();
	    	
	    	assert res != null;
	    	
	    	model.print(ast + " = " + res);
    	
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

	}
	

}
