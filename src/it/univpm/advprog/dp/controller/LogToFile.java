package it.univpm.advprog.dp.controller;

import java.io.PrintStream;

import it.univpm.advprog.dp.model.Model;

public class LogToFile extends Command {

	@Override
	public void execute(Model model) {
	
		try {
			String[] last = model.splitLastInput();
			
			PrintStream out = System.out;

			// :log
			// :log pippo.txt
			if (last.length > 1) {
				out = new PrintStream(last[1]);
			}
			
			model.setOutput(out);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
