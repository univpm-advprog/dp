package it.univpm.advprog.dp.controller;

import it.univpm.advprog.dp.model.Model;

public abstract class Command {
	public abstract void execute(Model model) ;
}
