package it.univpm.advprog.dp.controller;

import it.univpm.advprog.dp.model.Model;
import it.univpm.advprog.dp.model.Model.MODE;

public class SelectMode extends Command {

	@Override
	public void execute(Model model) {
		try
		{
			String[] last = model.splitLastInput();
			
			// this command has a name and an argument
			assert last.length == 2;
			
			if (last[1].equals("int") || last[1].equals("integer")) {
				model.setMode(MODE.integer);
			} else if (last[1].equals("array")) {
				model.setMode(MODE.array);
			} else {
				throw new RuntimeException("Argument of SelectMode not recognized: '" + last[1] + "'");
			}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

}
