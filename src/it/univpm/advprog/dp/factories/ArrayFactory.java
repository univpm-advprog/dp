package it.univpm.advprog.dp.factories;

import java.util.stream.Stream;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.Operation;
import it.univpm.advprog.dp.expressions.arrays.ArrayMinus;
import it.univpm.advprog.dp.expressions.arrays.ArrayMultiply;
import it.univpm.advprog.dp.expressions.arrays.ArrayPlus;
import it.univpm.advprog.dp.expressions.arrays.ArrayValue;

public class ArrayFactory extends Factory {

	@Override
	public ArrayValue createValue(String text) {
		
		String textParts[] = text.split(",");
		Integer intParts[] = Stream.of(textParts).map(Integer::parseInt).toArray(Integer[]::new);
		
		return new ArrayValue(intParts);
	}

	public ArrayValue createValue(Integer rawValue) {
		Integer[] intParts = { rawValue } ;
		return new ArrayValue(intParts);
	}
	
	@Override
	public Operation<Integer[]> createOperation(String text, Expression ... arguments) throws Exception {
		switch (text) {
		case "+" : 
			return new ArrayPlus(arguments);
		case "-" : 
			return new ArrayMinus(arguments);
		case "*" :
			return new ArrayMultiply(arguments);
		default:
			throw new Exception("Operation (" + text + ") is not recognized");	
		}
	}
}
