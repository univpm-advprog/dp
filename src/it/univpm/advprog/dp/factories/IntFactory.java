package it.univpm.advprog.dp.factories;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.Operation;
import it.univpm.advprog.dp.expressions.integers.IntMinus;
import it.univpm.advprog.dp.expressions.integers.IntMultiply;
import it.univpm.advprog.dp.expressions.integers.IntPlus;
import it.univpm.advprog.dp.expressions.integers.IntValue;

public class IntFactory extends Factory {

	@Override
	public IntValue createValue(String text) {
		return new IntValue(Integer.parseInt(text));
	}

	
	public IntValue createValue(Integer rawValue) {
		return new IntValue(rawValue);
	}

	
	@Override
	public Operation<Integer> createOperation(String text, Expression ... arguments) throws Exception {
		switch (text) {
		case "+" : 
			return new IntPlus(arguments);
		case "-" : 
			return new IntMinus(arguments);
		case "*" :
			return new IntMultiply(arguments);
		default:
			throw new Exception("Operation (" + text + ") is not recognized");	
		}
	}
}
