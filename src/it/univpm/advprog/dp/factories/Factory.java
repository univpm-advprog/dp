package it.univpm.advprog.dp.factories;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.Operation;

public abstract class Factory {
	public abstract AValue createValue(String text);

	public abstract AValue createValue(Integer rawValue);

	
	public abstract Operation createOperation(String text, Expression ... arguments) throws Exception;
}
