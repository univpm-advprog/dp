package it.univpm.advprog.dp.model;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class Model {
	
	public enum MODE {
		integer, array
	};
	private MODE currentMode = MODE.integer;

	private List<String> history = new ArrayList<>();

	private PrintStream out = System.out;

	//private Set<IObserver> observers = new HashSet<IObserver>();	
	
	public void appendToHistory(String text) {
		this.history.add(text);
	}
	
	public List<String> getHistory() {
		return this.history;
	}
	
	public String getLastInput() throws Exception {
		int size = this.history.size();
		if (size == 0) {
			throw new Exception("Empty history");
		}
		
		return this.history.get(size-1);
	}
	
	public String[] splitLastInput() throws Exception {
		Pattern p = Pattern.compile("[ ]+");
		String last = this.getLastInput();
		
		return p.split(last);
	}
	
	public void clearHistory() {
		this.history.clear();
	}

	public void setMode(Model.MODE mode) {
		this.currentMode = mode;
	}
	
	public MODE getMode() {
		return this.currentMode;
	}

	public void print(String text) {
		this.out.println(text);
	}
	
	public void setOutput(PrintStream out) {
		this.out = out;
	}
	
	/*
	public void attach(IObserver observer) {
		this.observers.add(observer);
	}
	
	public void detach(IObserver observer) {
		this.observers.remove(observer);
	}
	
	public void notifyObservers() {
		for (IObserver observer: this.observers) {
			observer.update(this);
		}
	}
	 */
}
