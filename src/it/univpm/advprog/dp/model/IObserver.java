package it.univpm.advprog.dp.model;

public interface IObserver {
	public void update(Model model);
}
