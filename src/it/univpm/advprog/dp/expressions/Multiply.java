package it.univpm.advprog.dp.expressions;

public abstract class Multiply<T> extends Operation<T> {

	public Multiply(Expression<T> ... arguments) {
		super("*", arguments);
	}

	@Override
	public void acceptVisitor(Visitor v) {
		v.visitMultiply(this);
	}
	
}
