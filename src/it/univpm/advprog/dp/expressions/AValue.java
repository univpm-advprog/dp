package it.univpm.advprog.dp.expressions;

public class AValue<T> extends Expression<T> {
	protected T value = null;
	
	public AValue(T value) {
		this.value = value;
	}
	
	public T getValue() {
		
		return this.value;
	}
	
	public AValue<T> evaluate() {
		return this;
	}
	
	
	public String toString() {
		return this.value.toString();
	}

	@Override
	public void acceptVisitor(Visitor v) {
		v.visitValue(this);
	}
}
