package it.univpm.advprog.dp.expressions.integers;

import it.univpm.advprog.dp.expressions.AValue;

public class IntValue extends AValue<Integer> {

	public IntValue(Integer value) {
		super(value);
	}

}
