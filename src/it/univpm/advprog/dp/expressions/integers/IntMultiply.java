package it.univpm.advprog.dp.expressions.integers;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.Multiply;
import it.univpm.advprog.dp.expressions.Operation;
import it.univpm.advprog.dp.expressions.Plus;

public class IntMultiply extends Multiply<Integer> {

	public IntMultiply(Expression<Integer> ... arguments) {
		super(arguments);
	}
}
