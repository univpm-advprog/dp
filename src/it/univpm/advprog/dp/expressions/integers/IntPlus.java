package it.univpm.advprog.dp.expressions.integers;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.Operation;
import it.univpm.advprog.dp.expressions.Plus;

public class IntPlus extends Plus<Integer> {

	public IntPlus(Expression<Integer> ... arguments) {
		super(arguments);
	}
	/*
	@Override
	public Integer getValue() {
		Integer result = 0;
		
		for (Expression<Integer> curr : this.arguments) {
			result += curr.getValue();
		} 
		
		return result;
	}

	@Override
	public AValue<Integer> evaluate() {
		return new IntValue(this.getValue());
	}*/
}
