package it.univpm.advprog.dp.expressions.integers;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.Minus;

public class IntMinus extends Minus<Integer> {

	public IntMinus(Expression<Integer> ... arguments) {
		super(arguments);
	}
	/*
	@Override
	public Integer getValue() {
		if (this.arguments.length == 0) {
			throw new RuntimeException("The IntMinus operation requires at least 1 argument");
		}
		
		Integer result = this.arguments[0].getValue(); 
		
		for (int pos = 1; pos < this.arguments.length; pos++) {
			result -= this.arguments[pos].getValue();
		} 
		
		return result;
	}

	@Override
	public AValue<Integer> evaluate() {
		return new IntValue(this.getValue());
	}*/
}
