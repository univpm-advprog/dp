package it.univpm.advprog.dp.expressions;

import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Visitor<T> {
	Logger logger = Logger.getAnonymousLogger();

	public abstract void reset();
	public abstract void visitValue(AValue<T> v);
	public abstract void visitPlus(Plus<T> o);
	public abstract void visitMinus(Minus<T> o);
	public abstract void visitMultiply(Multiply<T> o);
	
	public void logError(String message) {
		this.logger.log(Level.SEVERE, message);
	}
	
	public void logError(String message, Exception e) {
		this.logger.log(Level.SEVERE, message, e);
	}
	

}
