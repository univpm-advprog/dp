package it.univpm.advprog.dp.expressions;

public abstract class Plus<T> extends Operation<T> {

	public Plus(Expression<T> ... arguments) {
		super("+", arguments);
	}

	@Override
	public void acceptVisitor(Visitor v) {
		v.visitPlus(this);
	}
	
}
