package it.univpm.advprog.dp.expressions.parser;

import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.factories.Factory;

public abstract class Parser {
	Factory factory;
	
	public void setFactory(Factory f)
	{
		this.factory = f;
	}

	public abstract Expression parse(String text) throws Exception;

}
