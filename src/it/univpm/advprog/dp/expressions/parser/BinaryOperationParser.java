package it.univpm.advprog.dp.expressions.parser;


import java.util.regex.Pattern;

import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.Operation;
import it.univpm.advprog.dp.factories.Factory;

public class BinaryOperationParser extends Parser {

	
	public Expression parse(String text) throws Exception {
	
		if (this.factory == null) {
			throw new RuntimeException("You must specify a factory, before parsing");
		}
		
		Pattern p = Pattern.compile("[ ]+");
		String[] parts = p.split(text);
		
		if (parts.length == 1) {
			// parse a value
			return this.factory.createValue(parts[0]);
		} else if (parts.length == 3) {
			// parse a binary expression
			Expression left = this.parse(parts[0]);
			Expression right = this.parse(parts[2]);
			
			Operation op = this.factory.createOperation(parts[1], left, right);
			return op;
		} else {
			throw new Exception("Expression not recognized: " + text);
		}
		
		
	}
}
