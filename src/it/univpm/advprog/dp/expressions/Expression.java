package it.univpm.advprog.dp.expressions;

import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Expression<T> {
	Logger logger = Logger.getAnonymousLogger();
	
	public void logError(String message) {
		this.logger.log(Level.SEVERE, message);
	}
	
	public void logError(String message, Exception e) {
		this.logger.log(Level.SEVERE, message, e);
	}
	
	public abstract void acceptVisitor(Visitor v);
}


