package it.univpm.advprog.dp.expressions;

public abstract class Minus<T> extends Operation<T> {
	
	public Minus(Expression<T> ... arguments) {
		super("-", arguments);
	}

	@Override
	public void acceptVisitor(Visitor v) {
		v.visitMinus(this);
	}

}
