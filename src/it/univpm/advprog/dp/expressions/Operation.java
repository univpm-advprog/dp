package it.univpm.advprog.dp.expressions;

import java.util.stream.Stream;

public abstract class Operation<T> extends Expression<T> {
	protected String operator;
	protected Expression<T>[] arguments;
	
	//public Operation(String operator, Expression<T>[] arguments) {
	public Operation(String operator, Expression<T> ... arguments) {
		this.operator = operator;
		this.arguments = arguments;
	}
	
	public Expression<T>[] getArguments() {
		return this.arguments;
	}
	
	@Override
	public String toString() {
		// 1. converti this.arguments in uno stream
		// 2. mappa ogni elemento dello stream tramite la funzione Expression::toString
		// 3. costruisci lo stream di output
		// 4. converti lo stream in array di String
		String[] argumentsText = Stream.of(this.arguments).map(Expression::toString).toArray(String[]::new);
		
		// 5. crea una stringa separando tutti gli elementi di argumentsText dall'operatore
		return "(" + String.join(" " + this.operator + " ", argumentsText) + ")";
	}
	
}
