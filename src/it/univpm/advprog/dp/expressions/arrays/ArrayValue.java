package it.univpm.advprog.dp.expressions.arrays;

import java.util.stream.Stream;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Expression;

public class ArrayValue extends AValue<Integer[]> {

	public ArrayValue(Integer[] value) {
		super(value);
	}
	
	@Override
	public String toString() {
		String[] parts = new String[this.value.length];
		for (int pos = 0; pos < this.value.length; pos++) {
			parts[pos] = this.value[pos].toString();
		}
		
		return "[" + String.join(",", parts) + "]";
	}

}
