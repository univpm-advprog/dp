package it.univpm.advprog.dp.expressions.arrays;

import java.util.Arrays;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.Minus;
import it.univpm.advprog.dp.expressions.Operation;

public class ArrayMinus extends Minus<Integer[]> {
	
	public ArrayMinus(Expression<Integer[]> ... arguments) {
		super(arguments);
	}


	/*
	@Override
	public Integer[] getValue() {
		Integer[] result = new Integer[0];
		
		try {
			if (this.arguments.length == 0) {
				throw new RuntimeException("The ArrayMinus operation requires at least one argument");
			}
			
			result = this.arguments[0].getValue();
				
			for (int pos = 1; pos < this.arguments.length; pos++) {
				result = arrayMinus(result, this.arguments[pos].getValue());
			}
			 
		
		} catch (Exception e) {
			this.logError("The operation (" + this.toString() + ") contains an error", e);
		}
		return result;
	}

	@Override
	public AValue<Integer[]> evaluate() {
		return new ArrayValue(this.getValue());
	} */
}
