package it.univpm.advprog.dp.expressions.arrays;

import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.Multiply;

public class ArrayMultiply extends Multiply<Integer[]> {

	public ArrayMultiply(Expression<Integer[]> ... arguments) {
		super(arguments);
	}
	
}
