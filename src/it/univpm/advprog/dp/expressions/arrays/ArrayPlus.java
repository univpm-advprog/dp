package it.univpm.advprog.dp.expressions.arrays;

import it.univpm.advprog.dp.expressions.AValue;
import it.univpm.advprog.dp.expressions.Expression;
import it.univpm.advprog.dp.expressions.Plus;

public class ArrayPlus extends Plus<Integer[]> {

	public ArrayPlus(Expression<Integer[]> ... arguments) {
		super(arguments);
	}
	

	/*
	@Override
	public Integer[] getValue() {
		Integer[] result = null;
		
		try {
			for (Expression<Integer[]> curr : this.arguments) {
				result = arraySum(result, curr.getValue());
			} 
		
		} catch (Exception e) {
			this.logError("The operation (" + this.toString() + ") contains an error", e);
		}
		
		return result;
	}

	@Override
	public AValue<Integer[]> evaluate() {
		return new ArrayValue(this.getValue());
	}
*/
}
